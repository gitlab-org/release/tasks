## New team member

Welcome to the Delivery team!

The information below is meant to serve as a quick overview of both the company and the team resources.

Feel free to skip parts you are already familiar with, but keep in mind that some of the resources might have changed since you read them so a quick refresher could be useful.

## Company

GitLab is organised based on the product categories. Also have a look at the team org chart.

## Team

Like all teams we have a Delivery team section in the handbook. Take some time to read through, including all the linked resources.

Team member onboarding tasks
- [ ] Read the [Delivery Team](https://about.gitlab.com/handbook/engineering/infrastructure/team/delivery/) handbook section
- [ ] Read the [Platforms Stage](https://about.gitlab.com/handbook/engineering/infrastructure/platforms/) handbook section
- [ ] Review the [Infrastructure Department](https://about.gitlab.com/handbook/engineering/infrastructure/) handbook section
- [ ] Join the following Slack channels:
   - Delivery related channels
      - [ ] [#delivery-social](https://gitlab.slack.com/archives/C01QX84J6UR) (Delivery team social chats)
      - [ ] [#engineering-fyi](https://gitlab.slack.com/archives/CJWA4E9UG) (mandatory channel for engineering)
      - [ ] [#infrastructure-lounge](https://gitlab.slack.com/archives/CB3LSMEJV)  (Infrastructure department channels)
      - [ ] [#g_delivery](https://gitlab.slack.com/archives/CCFV016SV) (team channel)
      - [ ] [#infrastructure_platforms](https://gitlab.slack.com/archives/C02D1HQRTKQ) (platforms stage channel)
      - [ ] [#infrastructure_platforms_social](https://gitlab.slack.com/archives/C062T669RFD) (Platforms section social)
   - Release management channels
      - [ ] [#announcements](https://gitlab.slack.com/archives/C8PKBH3M5) (deployment feed, and general infrastructure announcements)
      - [ ] [#incident-management](https://gitlab.slack.com/archives/CB7P5CJS1)
      - [ ] [#f_upcoming_release](https://gitlab.slack.com/archives/C0139MAV672) (release management)
      - [ ] [#production](https://gitlab.slack.com/archives/C101F3796)
      - [ ] [#releases](https://gitlab.slack.com/archives/C0XM5UU6B)
- [ ] Read the [keeping yourself informed documentation](https://handbook.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/getting-started/keeping-yourself-informed/)
- [ ] [Update the team page](https://about.gitlab.com/handbook/git-page-update/#12-add-yourself-to-the-team-page) (If you haven't done it during your GitLab Onboarding Issue)

## Infrastructure tasks

- [ ] Review the handbook section on the [GitLab Sandbox](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/). This is essentially your portal to individual AWS and GCP accounts to use for development and testing.
  - Go through the steps to [set up a new CSP account](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project) for yourself.
  - If you'll be interacting with Terraform at all in your role, it's also worth reading through the section on [Terraform Environments](https://handbook.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#terraform-environments) for your individual CSP account.
- [ ] Even though Delivery does not develop the product directly, it's worth familiarising yourself with the [basic architecture of GitLab](https://docs.gitlab.com/ee/development/architecture.html), and having a passing familiarity with the [components that make up GitLab](https://docs.gitlab.com/ee/development/architecture.html#component-list). This is by no means required, but it can help to establish some context around the work that Delivery does to facilitate deployment and releases of the platform and its components.
- [ ] An important part of our work is to review code. Read [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html) to learn how we do it at GitLab.

## Manager onboarding tasks

For all team members:
- [ ] Open up a [machine setup onboarding release task issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/new?issuable_template=Onboarding-Delivery-Machine-Setup)
- [ ] Add new team member to delivery-team@gitlab.com Google group
- [ ] Add new team member to [Delivery group](https://gitlab.com/groups/gitlab-org/delivery) on GitLab.com
- [ ] Add new team member to [Ops Delivery group](https://ops.gitlab.net/groups/gitlab-com/delivery/-/group_members)
- [ ] Add new team member to [Dev Delivery group](https://dev.gitlab.org/groups/gitlab/delivery/-/group_members) with Owner rights
- [ ] Create an access request to add to the delivery-team slack group
- [ ] Add new team member to the Chatops Slack app: https://app.slack.com/app-settings/T02592416/A04P4TD0GKH/collaborators.
- [ ] Add new team member to the Release-Tools app: https://app.slack.com/app-settings/T02592416/A0385PGTMSQ/collaborators.
- [ ] Create access request to 'Release' and 'Build' 1Password Vaults
- [ ] Add to weekly team meeting
- [ ] Create a 1-1 document and schedule a recurring weekly meeting

Role specific access requests
- [ ] Create an access request for Managers, Infrastructure baseline entitlement
- [ ] Create an access request for Backend, Infrastructure baseline entitlement
- [ ] Create an access request for SRE, Infrastructure entitlement

## Release Manager training resources

You will be given a full Release Manager onboarding before you join the release manager rotation. The links below are for general awareness of what the Delivery team does.

- [ ] [Release Manager responsibilities](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/index.md#responsibilities)
- [ ] Overall release process can [be found here](https://about.gitlab.com/handbook/engineering/releases).
- [ ] [Deployments big picture](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/big_picture.md)
- [ ] Most of our work is on [release tools](https://gitlab.com/gitlab-org/release-tools), [k8s-workloads](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com), [Deployer](https://ops.gitlab.net/gitlab-com/gl-infra/deployer), or [Chatops](https://gitlab.com/gitlab-com/chatops)
- [ ] We track Delivery work in the [Delivery issue tracker](https://gitlab.com/gitlab-com/gl-infra/delivery/-/issues), and release issues in the [Release issue tracker](https://gitlab.com/gitlab-org/release/tasks/issues)
- [ ] The team current work can be found on our [Board](https://gitlab.com/gitlab-com/gl-infra/delivery/-/boards/1918862?label_name[]=Delivery%20team%3A%3ABuild). The board can also be filtered by team for [Delivery:Deployments](https://gitlab.com/gitlab-com/gl-infra/delivery/-/boards/1918862?label_name[]=Delivery%20team%3A%3ABuild&label_name[]=team%3A%3ADelivery%2DDeployments) and [Delivery:Releases](https://gitlab.com/gitlab-com/gl-infra/delivery/-/boards/1918862?label_name[]=Delivery%20team%3A%3ABuild&label_name[]=team%3A%3ADelivery%2DReleases).
- [ ] Work epics for [release velocity](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170), and for [Kubernetes](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/112)
- [ ] Browse the [release docs and runbooks](https://gitlab.com/gitlab-org/release/docs/#gitlab-release-process)

### Dashboards

* [Release manager dashboard](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/dashboard.md)
* [Omnibus versioning dashboard](https://dashboards.gitlab.net/d/CRNfDC7mk/gitlab-omnibus-versions?orgId=1&refresh=5m)
* [Kibana logs for release-tools](https://nonprod-log.gitlab.net/goto/1ad82080-5708-11ed-9af2-6131f0ee4ce6)

## Delivery:Releases / Delivery:Deployments specific onboarding

Based on the team, the manager will assign the right tasks to the new team member to learn the team's practices (e.g., Team Board, Team Retro, Team OKRs).
