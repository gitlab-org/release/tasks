## Release Management Shadowing Program On-Boarding

This is a training issue to onboard a trainee to the Release Management Shadowing Program. While shadowing, trainees will be paired to a specific Release Manager. During their shadowing slot, they will assist a Release Manager on their daily duties, joining sync training sessions, incidents and other activities related to Release Management, including deployments and releases.

### Trainee TODO

Trainee: `your_username`

Shadowing availability:

**Ideal Start Day/Week:** `targeted start date` 

**Timezone:**
- [ ] APAC
- [ ] EMEA
- [ ] AMER
- [ ] Other: `your_timezone`

**Day of the week*:**
- [ ] Monday
- [ ] Tuesday
- [ ] Wednesday
- [ ] Thursday
- [ ] Friday
- [ ] Saturday
- [ ] Sunday

**Time of day:**
- [ ] Morning
- [ ] Afternoon
- [ ] Evening


- [ ] Trainee: Assign yourself and `@mbursi` and `@dawsmith` to this issue.
- [ ] Trainee: Set up a sync or have an async chat with the Release Manager you are shadowing to go over any initial questions and your learning style
  and align on your expectations for your shadowing experience.

### Delivery EM TODO

Based on https://about.gitlab.com/release-managers/
- [ ] Assign a Release Manager on a rotation
  - `Release Manager @handle`


### First Tasks

- [ ] Trainee: Join the following Slack Channels:
    - #announcements - Shows deployments moving through the various environments.
    - #f_upcoming_release - Channel used by release managers to perform daily activities.
    - #g_delivery - Delivery group channel. A good place to ask any questions.
    - #incident-management - Any declared incidents will display in this channel. You will find links to the associated incident slack channel and issue.
    - #production - Contains a lot of production discussions, but also a place to see all feature flags being toggled on production.
    - #releases - Contains messages about monthly releases, patch releases, stable branch failures, and merge train.
- [ ] Trainee: Read through the [release guides](https://gitlab.com/gitlab-org/release/docs/blob/master/README.md)
- [ ] Trainee: Read through the [deployment and releases documentation](https://about.gitlab.com/handbook/engineering/deployments-and-releases/)
- [ ] Trainee: Read through the [release documentation](https://about.gitlab.com/handbook/engineering/releases/)
- [ ] Trainee: Read through the [deployment documentation](https://about.gitlab.com/handbook/engineering/deployments-and-releases/deployments/)
- [ ] Trainee: Read the deploy [docs](https://gitlab.com/gitlab-org/release/docs/tree/master#deployment)
- [ ] Trainee: Setup up a first day zoom with your RM shadow.  Feel free to bring any questions from what you have read.  While reading, also feel free to ask questions in the #g_delivery channel.
### Dashboards and issues to familiarize with and watch

These are reviewed weekly during the Delivery group weekly meeting.

- [ ] [Auto-deploy packages dashboard](https://dashboards.gitlab.net/d/delivery-auto_deploy_packages/delivery-auto-deploy-packages-information?orgId=1)
- [ ] [Deployment frequency and lead time](https://gitlab.com/gitlab-org/gitlab/-/pipelines/charts?chart=deployment-frequency)
- [ ] Familiarize with the [deployment blockers epic](https://docs.google.com/document/d/1OjEjjqeGFPFea5rN-z83osHgVPmAd9tT6FrPawZwB3o/edit#bookmark=id.yhuzt5ccbdyo)
- [ ] [Deployment Blockers dashboard](https://dashboards.gitlab.net/d/delivery-deployment_blockers/delivery3a-deployment-blockers?orgId=1)
- [ ] [Release management dashboard](https://dashboards.gitlab.net/d/delivery-release_management/delivery-release-management?orgId=1)

## Release Activities

- [ ] Release manager: Set up an initial call before the patch release week to explain the process and tooling
- [ ] Release manager / Shadow - Set up a daily shift call (15 min max) to go over the daily activities and the important actions
- [ ] Release manager: Ping the Shadow before starting with the patch release activities
- [ ] Shadow - Follow `f_upcoming_release_channel` and `#releases` for information.

### Tasks to shadow

Not every task will happen during your shadowing shift (for example a critical patch release). This is a non-exhaustive list of some major tasks to shadow when they occur.
You can find Release dates in the [handbook](https://about.gitlab.com/community/release-managers/).

* [ ] Patch release  - schedule a pairing session the Monday or Tuesday that week.  (Patch releases are Wednesdays before and after a Monthly release)
* [ ] Rollback practice
* [ ] Promoting a package
* [ ] Monthly release - schedule some pairing sessions to shadow the Friday and/or Monday just before the Monthly release.
* [ ] Deployment blocking incident
* [ ] Pick into autodeploy / manually push an MR through to production
* [ ] Declaring an incident
* [ ] Opening a release/tasks issue for a failure
* [ ] Execute the PDM pipeline
* [ ] Critical patch release
* [ ] Participate in a Weekly Delivery Metrics Review (held during Delivery Group Weekly call)

/label ~"onboarding" ~"group::delivery" ~"Release process"
