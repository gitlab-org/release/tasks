Title: Delivery Team Onboarding Issue 1 - Machine Setup

[Fill in name and start date]

### Welcome to your first onboarding issue!

We need to keep iterating on this checklist so please submit MR's for any improvements
that you can think of. The file is located in an issue template in the '[Delivery team issue template section](https://gitlab.com/gitlab-org/release/tasks/-/blob/master/.gitlab/issue_templates)'.

We have broken onboarding up into two separate issues: Machine Setup, Delivery Team Onboarding, and Release Manager Onboarding. They should be completed in that order, with the expectation that the Release Manager onboarding will take at least 3 months from your start date to complete. Take your time.

The goal for this first onboarding issue is to handle all of the installations and accesses you will need to have your machine ready to do the things you need to be able to do. At any time, feel free to ask your onboarding buddy for help, or reach out in #infrastructure-lounge.

- New team member = N
- Onboarder = O
- Manager = M

## Getting Started

1. [ ] O: cross-link general onboarding issue in the peopleops issue tracker
1. [ ] N: Install the GitLab Development Kit (GDK) by following the instructions in the [GDK documentation](https://gitlab.com/gitlab-org/gitlab-development-kit)

## Accounts and access

1. [ ] O: Check that by the start of Day 2 that an issue has been auto-created in the [Access Request Repo](https://gitlab.com/gitlab-com/team-member-epics/access-requests) by the employment bot.  This will create accounts for many of the services we use: AWS, dev, DO, GitLab.com admin, staging, GCP, Chef, Pager Duty
1. [ ] N: Complete the setup of both your [Yubikeys](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/yubikey.md) Note: We encourage you to get help if things are unclear or when you run into an issue
1. [ ] N: If you're going to be interacting with Kubernetes at all in your role. Note: We encourage you to get help if things are unclear or when you run into an issue:
    - [ ] [Setup 2 Yubikeys SSH keys](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/uncategorized/yubikey.md)
    - [ ] Add yourself as a new user to the collection of [Chef data bags](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/tree/master/data_bags/users), providing an SSH public key which will be propagated to the bastions. [Here's a sample MR for reference](https://gitlab.com/gitlab-com/gl-infra/chef-repo/-/commit/d9d0d4cd168a4373a9a85fdb8efad743ef3f7e01). Your baseline Access Request that your manager will have created for you also contains a to-do that requires an existing Chef Admin to make your new user an admin, which your manager will be able to help you with.
    - [ ] Go through the docs on [access to the bastion hosts](https://gitlab.com/gitlab-com/runbooks/-/tree/master/docs/bastions) to add relevant sections to your SSH config. Without these configs you won't be able to authenticate with our K8s clusters. The SSH username is the one you used in the `Chef data bags` above.
    - [ ] Go through the steps in the [k8s on-call setup doc](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/kube/k8s-oncall-setup.md).

## Manager onboarding tasks

For all team members:
1. [ ] M/O: Add new team member to delivery-team@gitlab.com Google group
1. [ ] M/O: Add new team member to [Delivery group](https://gitlab.com/groups/gitlab-org/delivery) on GitLab.com
1. [ ] M/O: Add new team member to [Ops Delivery group](https://ops.gitlab.net/groups/gitlab-com/delivery/-/group_members)
1. [ ] M/O: Create an access request to add to the delivery-team slack group
    - [ ] Add new team member to the Chatops Slack app: https://app.slack.com/app-settings/T02592416/A04P4TD0GKH/collaborators.
    - [ ] Add new team member to the Release-Tools app: https://app.slack.com/app-settings/T02592416/A0385PGTMSQ/collaborators.
    - [ ] Create access request to 'Release' and 'Build' 1Password Vaults
1. [ ] M/O: Add to weekly team meeting
1. [ ] M/O: Create a 1-1 document and schedule a recurring weekly meeting
1. [ ] M/O: Role specific access requests
    - [ ] Create an access request for Managers, Infrastructure baseline entitlement
    - [ ] Create an access request for Backend, Infrastructure baseline entitlement
    - [ ] Create an access request for SRE, Infrastructure entitlement

## Monitoring
1. [ ] N: Log in to the Grafana [private monitoring infrastructure](https://dashboards.gitlab.net/) using Google to create an account
1. [ ] N: Accept the invitation and configure [Pager Duty](https://gitlab.pagerduty.com/) timezone and phone number and other details

### Dashboards

- [Release manager dashboard](https://gitlab.com/gitlab-org/release/docs/-/blob/master/release_manager/dashboard.md)
- [Omnibus versioning dashboard](https://dashboards.gitlab.net/d/CRNfDC7mk/gitlab-omnibus-versions?orgId=1&refresh=5m)
- [Kibana logs for release-tools](https://nonprod-log.gitlab.net/goto/1ad82080-5708-11ed-9af2-6131f0ee4ce6)

### Acronyms and Terms to Know

- __CR__: Change Request
- __EOC__: Engineer On-Call
- __GPRD-CNY__: GitLab Production Canary
- __GPRD__: GitLab Production
- __PDM__: Post-Deploy Migration
- __RM__: Release Manager
- __SIRT__: Security Incident Response Team
